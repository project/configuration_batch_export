# Configuration Batch Export

The Configuration Batch Export module simplifies the process of exporting your
website's complete configuration using the Batch API.

For a detailed overview of the module, check out the
[project page](https://www.drupal.org/project/configuration_batch_export).

Feel free to report bugs, suggest features, or keep track of changes in the
[issue queue](https://www.drupal.org/project/issues/configuration_batch_export?categories=All).

## Installation

Install this module just like any other contributed Drupal module.
Need guidance? Visit
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Requirements

Make sure you have the following module installed:

- [Config Suite](https://www.drupal.org/project/config_suite)

## Usage

1. Navigate to Configuration » Development » Configuration synchronization.
2. Click on the "Batch Export" tab.
3. By default, the config files will be split into chunks of 40 config files.
Adjust this value based on your server resources.
A higher value speeds up the export but may increase server load.
4. Once the export is complete, your configuration will be automatically
downloaded in ZIP format.
5. Voila! You've efficiently exported your entire 
website configuration in seconds, thanks to the Batch API.

## Support

[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/zilloww)

If you enjoy my work and find it helpful,
you can support it buy offering me some fuel 🚀

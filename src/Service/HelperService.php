<?php

namespace Drupal\configuration_batch_export\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Helper service.
 */
class HelperService {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * Archive path.
   *
   * @var string
   */
  protected $archivePath;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger channel factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory, FileSystemInterface $fileSystem, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->configFactory = $configFactory;
    $this->fileSystem = $fileSystem;
    $this->loggerChannelFactory = $loggerChannelFactory;

    $this->archivePath = 'private://config_export/';
  }

  /**
   * Prepare export folder.
   */
  public function prepareExportFolder() {
    if (!file_exists($this->archivePath)) {
      mkdir($this->archivePath, 0777, TRUE);
    }
  }

  /**
   * Get temp folder path.
   *
   * @return string
   *   Temp folder path.
   */
  public function getTempFolderPath() {
    return $this->fileSystem->realpath($this->archivePath);
  }

  /**
   * Get archive name.
   *
   * @return string
   *   Archive name.
   */
  public function getArchiveName() {
    $siteName = $this->configFactory->get('system.site')->get('name');
    $siteName = str_replace(' ', '_', $siteName);
    $date = date('Y-m-d_H-i-s');

    return $siteName . '_' . $date . '.zip';
  }

  /**
   * Get archive real path.
   *
   * @param string $archiveName
   *   Archive name.
   *
   * @return string
   *   Archive real path.
   */
  public function getArchiveRealPath($archiveName) {
    $realpath = $this->fileSystem->realpath($this->archivePath);

    return $realpath . '/' . $archiveName;
  }

  /**
   * Get archive path.
   *
   * @return string
   *   Archive path.
   */
  public function getArchivePath() {
    return $this->archivePath . '/' . $this->getArchiveName();
  }

  /**
   * Create archive.
   *
   * @param string $archiveName
   *   Archive name.
   *
   * @return \ZipArchive
   *   Zip archive.
   *
   * @throws \Exception
   *   Exception.
   */
  public function createArchive($archiveName = 'config.zip') {
    $this->prepareExportFolder();

    $realpath = $this->fileSystem->realpath($this->archivePath);
    $zip = new \ZipArchive();
    $success = $zip->open($realpath . '/' . $archiveName, \ZipArchive::CREATE);

    if (!$success) {
      $this->loggerChannelFactory('configuration_batch_export')->error('Cannot create zip archive in @archivePath/@archiveName', ['@archivePath' => $this->archivePath, '@archiveName' => $archiveName]);
      throw new \Exception("Cannot create zip archive");
    }

    return $zip;
  }

  /**
   * Get archive.
   *
   * @return \ZipArchive
   *   Zip archive.
   */
  public function getArchive() {
    $realpath = $this->fileSystem->realpath($this->archivePath);
    $zip = new \ZipArchive();
    $zip->open($realpath . '/' . $this->getArchiveName(), \ZipArchive::CREATE);

    return $zip;
  }

  /**
   * Get config names.
   *
   * @return array
   *   Config names.
   */
  public function getConfigNames() {
    return $this->configFactory->listAll();
  }

  /**
   * Get config data.
   *
   * @param string $configName
   *   Config name.
   *
   * @return array
   *   Config data.
   */
  public function getConfigData($configName) {
    $config = $this->configFactory->getEditable($configName);
    $configData = $config->getRawData();

    return $configData;
  }

}

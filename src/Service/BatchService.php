<?php

namespace Drupal\configuration_batch_export\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\Yaml\Yaml;

/**
 * Batch service.
 */
class BatchService {

  use StringTranslationTrait;

  /**
   * Helper service.
   *
   * @var \Drupal\configuration_batch_export\Service\HelperService
   */
  protected $helperService;

  /**
   * Constructor.
   *
   * @param \Drupal\configuration_batch_export\Service\HelperService $helperService
   *   Helper service.
   */
  public function __construct(HelperService $helperService) {
    $this->helperService = $helperService;
  }

  /**
   * Starts the batch process.
   *
   * @param int $configs_per_chunk
   *   Number of configurations per chunk.
   */
  public function batchStart($configs_per_chunk) {
    $configNames = $this->helperService->getConfigNames();

    $chunks = array_chunk($configNames, $configs_per_chunk);

    $operations = [];
    foreach ($chunks as $chunk) {
      $operations[] = [
        '\Drupal\configuration_batch_export\Service\BatchService::batchOperationProcessChunk',
            [$chunk],
      ];
    }

    $operations[] = [
      '\Drupal\configuration_batch_export\Service\BatchService::batchOperationProcessArchive',
          [],
    ];

    $batch = [
      'title' => $this->t('Configuration Batch Export'),
      'operations' => $operations,
      'finished' => '\Drupal\configuration_batch_export\Service\BatchService::batchOperationFinished',
      'init_message' => $this->t('Starting export of the entire configuration...'),
      'error_message' => $this->t('An error ocurred while exporting the entire configuration.'),
      'progress_message' => $this->t('Processed and exported @current out of @total config files. Estimated time: @estimate. Elapsed time: @elapsed.'),
    ];

    batch_set($batch);
  }

  /**
   * Batch operation: Process chunk.
   *
   * @param array $chunk
   *   Chunk of configuration names.
   * @param array $context
   *   Batch context.
   */
  public static function batchOperationProcessChunk($chunk, &$context) {
    $context['message'] = t('Processing configurations...');

    foreach ($chunk as $configName) {
      $configData = \Drupal::configFactory()->get($configName)->getRawData();
      $ymlData = Yaml::dump($configData, 10, 2);

      $context['results']['configYmlFiles'][$configName] = $ymlData;
    }
  }

  /**
   * Batch operation: Process archive.
   *
   * @param array $context
   *   Batch context.
   */
  public static function batchOperationProcessArchive(&$context) {
    $context['message'] = t('Creating archive...');

    $archiveName = \Drupal::service('configuration_batch_export.helper')->getArchiveName();
    $zip = \Drupal::service('configuration_batch_export.helper')->createArchive($archiveName);

    foreach ($context['results']['configYmlFiles'] as $configName => $ymlData) {
      $zip->addFromString($configName . '.yml', $ymlData);
    }

    $zip->close();

    $context['results']['zipArchivePath'] = \Drupal::service('configuration_batch_export.helper')->getArchiveRealPath($archiveName);
  }

  /**
   * Batch operation: Finished.
   *
   * @param bool $success
   *   Success flag.
   * @param array $results
   *   Batch results.
   * @param array $operations
   *   Batch operations.
   */
  public static function batchOperationFinished($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      $messenger->addMessage(t('Batch export of the entire configuration finished.'));
    }
    else {
      $messenger->addMessage(t('An unknown error ocurred while exporting the website config.'), 'error');
    }
  }

}

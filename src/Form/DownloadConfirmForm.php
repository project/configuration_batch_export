<?php

namespace Drupal\configuration_batch_export\Form;

use Drupal\configuration_batch_export\Service\HelperService;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Defines a confirmation form to confirm the download of the archive.
 */
class DownloadConfirmForm extends ConfirmFormBase {

  use StringTranslationTrait;

  /**
   * The configuration batch export helper service.
   * 
   * @var \Drupal\configuration_batch_export\Service\HelperService
   */
  protected $helperService;

  /**
   * Constructs a new DownloadConfirmForm object.
   * 
   * @param \Drupal\configuration_batch_export\Service\HelperService $helperService
   *  The configuration batch export helper service.
   */
  public function __construct(HelperService $helperService) {
    $this->helperService = $helperService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('configuration_batch_export.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $exportFolderPath = $this->helperService->getTempFolderPath();
    if (isset($exportFolderPath) && !empty($exportFolderPath)) {
      $files = scandir($exportFolderPath);
    }
    $files = $files ?? [];
    if ($files !== NULL) {
      $files = array_diff($files, ['.', '..']);
    }

    $messenger = \Drupal::messenger();
    $logger = \Drupal::logger('configuration_batch_export');

    if (count($files) == 0) {
      $messenger->addError($this->t('No archive found. Please export your configuration again.'));
      return;
    }
    elseif (count($files) > 1) {
      $messenger->addError($this->t('An error ocurred while processing your archive. Please, remove all the the archives present in the folder @folder and export your configuration again.', ['@folder' => $exportFolderPath]));
      $logger->error('More than one archive found in the folder @exportFolderPath. Please, remove all the archives and export your configuration again.', ['@exportFolderPath' => $exportFolderPath]);
      return;
    }
    else {
      $archiveName = $files[2];

      $archivePath = $this->helperService->getArchiveRealPath($archiveName);
      $response = new BinaryFileResponse($archivePath);

      $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $archiveName
            );

      $response->send();

      $logger->info('Archive "@archiveName" downloaded successfully.', ['@archiveName' => $archiveName]);

      unlink($archivePath);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "configuration_batch_export_download_confirm_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('configuration_batch_export.export');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Download your archive');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Your configuration has been exported successfully. Do you want to download the archive?');
  }

}

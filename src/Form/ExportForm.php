<?php

namespace Drupal\configuration_batch_export\Form;

use Drupal\configuration_batch_export\Service\BatchService;
use Drupal\configuration_batch_export\Service\HelperService;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Export form.
 */
class ExportForm extends FormBase {

  /**
   * Batch service.
   *
   * @var \Drupal\configuration_batch_export\Service\BatchService
   */
  protected $batchService;

  /**
   * Helper service.
   *
   * @var \Drupal\configuration_batch_export\Service\HelperService
   */
  protected $helperService;

  /**
   * Default number of configurations per chunk.
   *
   * @var int
   */
  protected $defaultConfigsPerChunk;

  /**
   * Constructor.
   *
   * @param \Drupal\configuration_batch_export\Service\BatchService $batchService
   *   Batch service.
   * @param \Drupal\configuration_batch_export\Service\HelperService $helperService
   *   Helper service.
   */
  public function __construct(BatchService $batchService, HelperService $helperService) {
    $this->batchService = $batchService;
    $this->helperService = $helperService;
    $this->defaultConfigsPerChunk = 40;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('configuration_batch_export.batch'),
      $container->get('configuration_batch_export.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'configuration_batch_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $username = NULL) {
    $form['configs_per_chunk'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of configurations per chunk'),
      '#description' => $this->t('This defines the number of configuration files that will be processed in each batch operation. <br /><em>(Default: @defaultConfigsPerChunk, Min: 1, Max: 200)</em>', ['@defaultConfigsPerChunk' => $this->defaultConfigsPerChunk]),
      '#default_value' => $this->defaultConfigsPerChunk,
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 200,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $configs_per_chunk = $form_state->getValue('configs_per_chunk');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->batchService->batchStart($form_state->getValue('configs_per_chunk'));
    $form_state->setRedirect('configuration_batch_export.download_archive');
  }

}
